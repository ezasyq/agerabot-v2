<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    // di frontend pakai name = q
    public function robotSearch(Request $request) {
        if($request->has('q')) {
            $cari = $request->q;
            $results = DB::table('robots')
            ->where('robotLongName', 'LIKE', '%'.$cari.'%')->get();
            // ->paginate(12);
            
            return view('pages.robot', compact('results'));
        }
    }
    public function robotCompare(Request $request) {
        if($request->has('q')) {
            $cari = $request->q;
            $results = DB::table('robots')
            ->where('robotLongName', 'LIKE', '%'.$cari.'%')->get();
            // ->paginate(12);
            
            return view('pages.robot', compact('results'));
        }
    }
    // public function robotCompare(Request $request) {
    //     if($request->has('q')) {
    //         $cari = $request->q;
    //         $results = DB::table('robots')->where('robotLongName', 'LIKE', '%'.$cari.'%')->get();
    //         return view('pages.robot', compact('results'));
    //     }
    // }
}
