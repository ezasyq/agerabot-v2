<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class PagesController extends Controller
{
    public function homePage()
    {
        $robot = DB::table('robots')->get();
        return view('pages.homepage', compact('robot'));
    }

    public function robotPage()
    {
        
        $robot = DB::table('robots')->paginate(12);
		// dd($robot);
        return view('pages.robot', compact('robot'));
    }

    public function subsPage()
    {
        return view('pages.subscription');
    }

	public function registerPage()
	{
		return view('pages.register');
	}

    // best Robot
    public function statistic_pair($type,$periode,$pair){
    // public function statistic_pair(){
        // $type = 
		$pullRobot = DB::table('robotStatistic')->distinct()->select('stat_robot')->get();

		foreach ($pullRobot as $robot) {
			$pullPair = DB::table('robotStatistic')->where('stat_robot',$robot->stat_robot)->where('stat_pair',$pair)->orderby('id','desc')->first();
			$data[] = [
				'robot' => $robot->stat_robot,
				'pair' => $pair,
				'last_percent' => $pullPair->stat_lastten,
				'last_total' => $pullPair->stat_totalten,
				'week_percent' => $pullPair->stat_weekly,
				'week_total' => $pullPair->stat_totalweekly,
				'month_percent' => $pullPair->stat_monthly,
				'month_total' => $pullPair->stat_totalmonthly,
				'year_percent' => $pullPair->stat_yearly,
				'year_total' => $pullPair->stat_totalyearly,
			];
		}

		$sort = $periode."_".$type;

		$array = collect($data)->sortBy($sort)->reverse()->toArray();
        // dd($array);
		return response()->json($array);
	}


	public function statistic_robot($type,$periode,$robot){
		$pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();

		foreach ($pullPair as $pair) {
            
			$pullRobot = DB::table('robotStatistic')->where('stat_robot',$robot)->where('stat_pair',$pair->stat_pair)->orderby('id','desc')->first();
			$data[] = [
				'robot' => $robot,
				'pair' => $pair->stat_pair,
				'last_percent' => $pullRobot->stat_lastten,
				'last_total' => $pullRobot->stat_totalten,
				'week_percent' => $pullRobot->stat_weekly,
				'week_total' => $pullRobot->stat_totalweekly,
				'month_percent' => $pullRobot->stat_monthly,
				'month_total' => $pullRobot->stat_totalmonthly,
				'year_percent' => $pullRobot->stat_yearly,
				'year_total' => $pullRobot->stat_totalyearly,
			];
		}

		$sort = $periode."_".$type;

		$array = collect($data)->sortBy($sort)->reverse()->toArray();
		return response()->json($array);
	}

	public function statistic_all($type,$periode){
        // performance
		$pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
		$pullRobot = DB::table('robotStatistic')->distinct()->select('stat_robot')->get();
		foreach ($pullPair as $pair) {
			foreach ($pullRobot as $robot) {
				$pullData = DB::table('robotStatistic')->where('stat_robot',$robot->stat_robot)->where('stat_pair',$pair->stat_pair)->orderby('id','desc')->first();
				$data[] = [
					'robot' => $robot->stat_robot,
					'pair' => $pair->stat_pair,
					'last_percent' => $pullData->stat_lastten,
					'last_total' => $pullData->stat_totalten,
					'week_percent' => $pullData->stat_weekly,
					'week_total' => $pullData->stat_totalweekly,
					'month_percent' => $pullData->stat_monthly,
					'month_total' => $pullData->stat_totalmonthly,
					'year_percent' => $pullData->stat_yearly,
					'year_total' => $pullData->stat_totalyearly,
				];
			}
		}
		return response()->json($data);
	}

	public function bestRobot($type,$periode){
        // untuk mengambil yang terbaik
		$pullPair = DB::table('robotStatistic')->distinct()->select('stat_pair')->get();
		foreach ($pullPair as $pair) {
			$result = $this->statistic_pair($type,$periode,$pair->stat_pair);
			$data = $result->getData();
			$urut = [];
			foreach ($data as $key => $value) {
				$urut[] = $value;
			}

			$robot = DB::table('robots')->where('id',$urut[0]->robot)->first();

			$anu[] = ['pair' => $pair->stat_pair, 'robotName' => $robot->robotLongName, 'timeframe' => $robot->robotAttribute5, 'ytd' => $urut[0]->year_total,'data' => $urut[0]];
		}

		$best = collect($anu)->sortBy('ytd')->reverse()->toArray();
		return view('pages.statistic-best-robot',compact('best'));
	}
}
