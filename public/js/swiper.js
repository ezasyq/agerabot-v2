var swiper = new Swiper('.swiper-robot', {
    slidesPerView: 3,
    grabCursor: true,
    spaceBetween: 30,
    breakpoints: {
      '@0.00': {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      '@0.75': {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      '@1.00': {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      '@1.50': {
        slidesPerView: 3,
        spaceBetween: 50,
      },
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });
      
let swiperTestimonial = new Swiper('.swiper-testimonial', {
    slidesPerView: 1,
    spaceBetween: 30,
    grabCursor: true,
    loop: true,
    speed: 2000,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    autoplay: {
    delay: 5000,
  },
});