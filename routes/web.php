<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@homePage')->name('homePage');
Route::get('/robot', 'PagesController@robotPage')->name('robotPage');
Route::get('/subscription', 'PagesController@subsPage')->name('subsPage');
Route::get('/register', 'PagesController@registerPage')->name('registerPage');

// routelib
Route::get('/lib/statistic/pair', 'PageController@statistic_pair')->name('statisticPair');
Route::get('/lib/statistic/robot', 'PagesController@statistic_robot')->name('statisticPair');
Route::get('/bestRobot', 'PagesController@bestRobot')->name('bestRobot');

// Route Search
Route::get('/search/robot', 'SearchController@robotSearch')->name('robotSearch');
// Route::get('/search/robot', 'SearchController@robotSearch')->name('robotSearch');