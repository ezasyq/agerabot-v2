@extends('app')

@section('content')
<div>
    <div class="hero">
      <div class="hero-txt">
        <h1>All in one step</h1>
        <h5>There's a lot of opportunities for you to gain profit when robot-trading. Take this opportunity to learn more about the trading robots on agerabot.com</h5>
        <a href="{{ route('subsPage') }}" type="button">
          <button class="hero-btn">
            SUBSCRIBE NOW
          </button>
        </a>
      </div>
      <div class="ungu-ungu"></div>
    </div>
</div>

<div class="container text-center section-home">
    <h2>Are you ready to explore what Trading Robot can do?</h2>
    <h4>Using Trading robots, you could maximize their 3 advantages</h4>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <img src="{{ asset('./images/icon1.svg') }}" alt="icon" class="section-home-icon">
                  <h5>NO MARKET RESEARCH NEEDED</h5>
                  <p>Everything has been analised and well calculated by our trading robots.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <img src="{{ asset('./images/icon2.svg') }}" alt="icon" class="section-home-icon">
                  <h5>SAVE TRADING TIME</h5>
                  <p>Trading robots will automatically execute your transactions.</p>
                </div>
              </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <img src="{{ asset('./images/icon3.svg') }}" alt="icon" class="section-home-icon">
                  <h5>TRADING'S PSYCHOLOGY AVOIDED</h5>
                  <p>Your transactions become more neat and keep the over-transaction away</p>
                </div>
              </div>
        </div>
    </div>
</div>

{{-- <div class="section-robot-bg">
    <div class="container section-robot">
        <h2>Choose your Robot Trading!</h2>
        <div class="row robot-slider">
            <div class="col-md-4 robot-slider-bg">
                <div class="text-right">
                    <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-slider-img">
                </div>
                <h3>Tach Down</h3>
                <p>Attack</p>
                <p>Defense</p>
                <p>Speed</p>
                <button class="robot-slider-btn">TRANSACTION NOW</button>
            </div>
            <div class="col-md-4 robot-slider-bg">
                <div class="text-right">
                    <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-slider-img">
                </div>
                <h3>Tach Down</h3>
                <p>Attack</p>
                <p>Defense</p>
                <p>Speed</p>
                <button class="robot-slider-btn">TRANSACTION NOW</button>
            </div>
            <div class="col-md-4 robot-slider-bg">
                <div class="text-right">
                    <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-slider-img">
                </div>
                <h3>Tach Down</h3>
                <p>Attack</p>
                <p>Defense</p>
                <p>Speed</p>
                <button class="robot-slider-btn">TRANSACTION NOW</button>
            </div>
        </div>
        <button class="robot-slider-detail">SHOW DETAIL</button>
    </div>
</div> --}}

<div class="container section-step">
    <h2>Bagaimana cara untuk Anda dapat menggunakan robot trading?</h2>
    <h4>Hanya dengan 3 langkah, Anda langsung dapat menikmati robot trading dengan mudah dan nyaman</h4>
    
    <div class="row" style="margin: 75px auto; row-gap: 75px">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <img src="{{ asset('./images/step1.svg') }}" alt="icon" class="section-step-icon">
                  <div class="step-flex">
                    <img src="{{ asset('./images/step1.png') }}" alt="icon" class="section-step-img">
                    <p>Register</p>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <img src="{{ asset('./images/step2.svg') }}" alt="icon" class="section-step-icon">
                  <div class="step-flex">
                    <img src="{{ asset('./images/step2.png') }}" alt="icon" class="section-step-img">
                    <p>Sewa Robot Trading</p>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <img src="{{ asset('./images/step3.svg') }}" alt="icon" class="section-step-icon">
                  <div class="step-flex">
                    <img src="{{ asset('./images/step3.png') }}" alt="icon" class="section-step-img">
                    <p>Install Robot Trading</p>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <a href="{{ route('subsPage') }}">
      <button class="subscribe-btn">SUBSCRIBE NOW</button>
    </a>
</div>

{{-- Testimonial --}}
<section class="col-md-12 p-5">
  <div class="swiper-container swiper-testimonial">
      <div class="swiper-wrapper">
          <div class="swiper-slide">
              <div class="container">
                  <div class="row d-flex">
                      <div class="col-md-12 col-lg-7 align-items-center">
                          <div class="testi-content_block">
                              <img src="{{ asset('images/quotes.png') }}" class="img-fluid" alt="#">
                              <h5>Sebagai trader aktif, saya sangat terbantu dengan situs agerabot ini karena selama ini saya harus membeli dengan harga yang mahal hanya untuk sebuah robot trading forex, sekarang dengan harga yang relatif sangat murah saya sudah bisa pakai robot dan terus menghasilkan keuntungan setiap bulannya.</h5>
                              <span>Agus Hariadi</span>
                              <p>Profesional Trader, Jakarta</p>
                          </div>
                      </div>
                  </div>
                  <div class="testi-img_wrap bg-testi-sewarobot">
                      <img src="{{ asset('images/model-1.png') }}" class="img-fluid" alt="#" width="250">
                  </div>
              </div>
          </div>
          <div class="swiper-slide">
              <div class="container">
                  <div class="row d-flex">
                      <div class="col-md-12 col-lg-7 align-items-center">
                          <div class="testi-content_block">
                              <img src="{{ asset('images/quotes.png') }}" class="img-fluid" alt="#">
                              <h5>Harga yang sesuai untuk sewa robot trading forex apalagi sudah termasuk VPS, robot trading forex dapat bekerja sepanjang hari 24 jam non-stop. Begitu ada peluang yang muncul, robot akan selalu siap siaga mengeksekusi order market kapanpun. Dengan kata lain, saya tidak perlu lagi khawatir terlewatkan peluang trading meskipun saya sibuk bekerja atau sedang beristirahat.</h5>
                              <span>Hendriansyah</span>
                              <p>Investor Property, Semarang</p>
                          </div>
                      </div>
                  </div>
                  <div class="testi-img_wrap bg-testi-sewarobot">
                      <img src="{{ asset('images/model-2.png') }}" class="img-fluid" alt="img" width="300">
                  </div>
              </div>
          </div>
          <div class="swiper-slide">
              <div class="container">
                  <div class="row d-flex">
                      <div class="col-md-12 col-lg-7 align-items-center">
                          <div class="testi-content_block">
                              <img src="{{ asset('images/quotes.png') }}" class="img-fluid" alt="#">
                              <h5>Awalnya saya hanya ingin mencoba robot trading forex karena ajakan teman yang sukses bertransaksi forex pakai robot, tetapi saya masih ragu-ragu untuk beli robot yang harganya relatif mahal apalagi saya masih pemula. Setelah mencoba sendiri robot dari situs agerabot ini akhirnya saya bisa membuktikan bahwa trading forex pakai robot itu tidak harus mahal.</h5>
                              <span>Budi Sudarsono</span>
                              <p>Karyawan Swasta, Surabaya</p>
                          </div>
                      </div>
                  </div>
                  <div class="testi-img_wrap bg-testi-sewarobot">
                      <img src="{{ asset('images/model-3.png') }}" class="img-fluid" alt="img" width="250">
                  </div>
              </div>
          </div>
      </div>
      <!-- Add Arrows -->
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
  </div>
</section>
<!--//END TESTIMONIAL -->

{{-- <h2>Choose your Robot Trading!</h2>
<div class="swiper-container" style="border: 1px solid red">
  <div class="swiper-wrapper">

    @for ($i = 0; $i < 6; $i++)
    <div class="swiper-slide">
      <div class="robot-carousel-bg">
        <div class="text-right">
            <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-carousel-img">
        </div>
        <h3>Tach Down</h3>
        <p>Attack</p>
        <p>Defense</p>
        <p>Speed</p>
        <button class="robot-carousel-btn">TRANSACTION NOW</button>
      </div>
    </div>
    @endfor
    
  </div>
  <!-- Add Arrows -->
  <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>
</div> --}}

@endsection

