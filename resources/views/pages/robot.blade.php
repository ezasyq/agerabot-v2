@extends('app')

@section('content')
    <div class="robot-hero">
        <div class="robot-hero-txt">
            <h2>Temukan Robot yang tepat untuk Anda</h2>
            <h5>Kami menyediakan berbagai jenis robot yang sesuai dengan karakter dan tipe trading Anda, dan pastinya robot kami memiliki performa yang bisa membantu Anda meraih profit!</h5>
        </div>
        <div class="robot-hero-overlay"></div>
    </div>

    <div class="container robot-menu-container">
        <div class="customize_solution">
            <ul class="tabs nav nav-justified">
                <li class="tab-link current nav-pill mt-2" href="tab-1">
                    <span class="ease-effect">Top 10</span> </li>
                <li class="tab-link nav-pill mt-2" href="tab-2">
                    <span class="ease-effect">Robot Detail List</span>
                </li>
                <li class="tab-link nav-pill mt-2" href="tab-3">
                    <span class="ease-effect">Robot Comparison</span>
                </li>
            </ul>
        </div>

        {{-- TOP 10 ROBOT --}}
        <div class="tab-content current" id="tab-1">
            <div class="top-ten-robot">
                <h2 class="text-center top-robot-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>

                <div class="row" style="row-gap: 50px; margin-top: 100px">

                    @for ($i = 1; $i <= 3; $i++)
                    <div class="top-robot-card card col-12 mb-3">
                        <div class="card-body">
                          <img src="{{ asset('./images/robot-hidden.png') }}" alt="" class="top-robot-img">
                          <h5>Subscribe untuk melihat</h5>
                          <button class="top-robot-btn">TRANSACTION NOW</button>
                          <img src="{{ asset('./images/robot-rank.svg') }}" alt="" class="top-robot-rank">
                          <h1 class="top-robot-number">{{ $i }}</h1>
                        </div>
                    </div>
                    @endfor

                    @for ($i = 4; $i <= 10; $i++)
                    <div class="top-robot-card-ten card col-12 mb-3">
                        <div class="card-body">
                          <img src="{{ asset('./images/top-robot.png') }}" alt="" class="top-robot-img">
                          <div class="top-robot-desc">
                            <h5>Tach Down</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                          </div>
                          <button class="top-robot-btn">SHOW DETAILS</button>
                          <img src="{{ asset('./images/robot-rank-2.svg') }}" alt="" class="top-robot-rank-2">
                          <h1 class="top-robot-number">{{ $i }}</h1>
                        </div>
                    </div>
                    @endfor

                    
                </div>

                <div class="text-center mt-5">
                    <a href="{{ route('subsPage') }}">
                        <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
                    </a>
                    <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
                </div>

            </div>
        </div>

        {{-- ROBOT DETAIL LIST --}}
        <div class="tab-content" id="tab-2">
            <h2 class="text-center robot-detail-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>

            <div class="search-filter">
                <div class="filter">
                    Sort By: <input type="text" class="filter-box">
                </div>
                <div class="search">
                    <i class="fas fa-search search-icon"></i>
                    <input type="search" class="search-box" placeholder="Search...">
                </div>
            </div>

            <div class="row robot-detail-slider">

                @foreach ($robot as $robots)
                <div class="col-md-4 robot-detail-slider-bg text-center" style="margin-top: 100px; margin-bottom: 50px">
                    <div class="text-right">
                        <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-detail-slider-img">
                    </div>
                    <h3>{{ $robots->robotLongName }}</h3>
                    <div class="robot-stat">
                        <p>Attack</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="10" style="width: {{ $robots->robotAttribute1*10 . '%' }}"></div>
                        </div>
                        <p>{{ $robots->robotAttribute1 . '/10' }}</p>
                    </div>
                    <div class="robot-stat">
                        <p>Defense</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="10" style="width: {{ $robots->robotAttribute2*10 . '%' }}"></div>
                        </div>
                        <p>{{ $robots->robotAttribute2 . '/10' }}</p>
                    </div>
                    <div class="robot-stat">
                        <p>Speed</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="10" style="width: {{ $robots->robotAttribute3*10 . '%' }}"></div>
                        </div>
                        <p>{{ $robots->robotAttribute3 . '/10' }}</p>
                    </div>
                    <button class="robot-detail-slider-btn">SHOW DETAILS</button>
                </div>
                @endforeach

                {{ $robot->links() }}



                {{-- @for ($i = 0; $i < 9; $i++)
                <div class="col-md-4 robot-detail-slider-bg text-center" style="margin-top: 100px; margin-bottom: 50px">
                    <div class="text-right">
                        <img src="{{ asset('./images/robot.png') }}" alt="" class="robot-detail-slider-img">
                    </div>
                    <h3>Tach Down</h3>
                    <div class="robot-stat">
                        <p>Attack</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <p>7/10</p>
                    </div>
                    <div class="robot-stat">
                        <p>Defense</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <p>7/10</p>
                    </div>
                    <div class="robot-stat">
                        <p>Speed</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <p>7/10</p>
                    </div>
                    <button class="robot-detail-slider-btn">SHOW DETAILS</button>
                </div>
                @endfor --}}
            </div>
        
            <div class="text-center">
                <a href="{{ route('subsPage') }}">
                    <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
                </a>
                <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
            </div>



        </div>

        {{-- ROBOT COMPARISON --}}
        <div class="tab-content" id="tab-3">
            <div>
                <h2 class="text-center robot-compare-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>
            </div>

            <div class="robot-compare-search">
                <div class="search">
                    <i class="fas fa-search search-icon"></i>
                    <input type="search" class="search-box" placeholder="Search...">
                </div>
                <div class="search">
                    <i class="fas fa-search search-icon"></i>
                    <input type="search" class="search-box" placeholder="Search...">
                </div>
            </div>

            <div class="compare-btn">
                <button>COMPARE</button>
            </div>

            <div class="robot-comparison">
                <h3>Popular Robot Comparisons</h3>
                <div class="row robot-comparison-card">

                    @for ($i = 0; $i < 4; $i++)
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                              <img src="{{ asset('images/robot-compare-1.png') }}" alt="robot">
                              <img src="{{ asset('images/robot-compare-2.png') }}" alt="robot">
                            </div>
                            <div class="name-robot-compare">
                                <h5>Grid Lok</h5>
                                <h5>Blek Jac</h5>
                            </div>
                        </div>
                    </div>
                    @endfor

                    
                </div>
            </div>

            <div class="text-center">
                <a href="{{ route('subsPage') }}">
                    <button class="subscribe-btn mb-5">SUBSCRIBE NOW</button>
                </a>
                <button class="subscribe-btn mb-5">LEARN HOW TO USE</button>
            </div>

        </div>

        {{-- Menu Bawah --}}
        <div class="customize_solution my-5">
            <ul class="tabs nav nav-justified">
                <li class="tab-link current nav-pill mt-2" href="tab-1">
                    <span class="ease-effect">Top 10</span> </li>
                <li class="tab-link nav-pill mt-2" href="tab-2">
                    <span class="ease-effect">Robot Detail List</span>
                </li>
                <li class="tab-link nav-pill mt-2" href="tab-3">
                    <span class="ease-effect">Robot Comparison</span>
                </li>
            </ul>
        </div>
        {{-- End Menu Bawah --}}

    </div>
@endsection