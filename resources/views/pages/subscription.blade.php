@extends('app')

@section('content')
<div class="robot-hero">
    <div class="robot-hero-txt">
        <h2>Lorem ipsum dolor sit amet</h2>
        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h5>
    </div>
    <div class="subs-hero-overlay"></div>
</div>

<div class="container subs-price-container">
    <div class="row"> <!-- Start Price Card -->

        <div class="col-md-4">
            <div class="price-card">
                  <div class="price-header">
                      <h5>1 Month</h5>
                      <div class="d-flex" style="align-items: center; justify-content: center">
                          <sup>US$</sup><h1>4.99</h1>
                      </div>
                  </div>

                  <div class="price-feature">
                      <h5 class="feature-bg">All Robot of your choice</h5>
                      <h5>Top 3 Robot for trading recommendation</h5>
                      <h5 class="feature-bg">Daily Signal Recommendation</h5>
                  </div>

                  <div class="text-center">
                      <button class="price-btn">SUBSCRIBE NOW</button>
                  </div>
              </div>
        </div>
        <div class="col-md-4">
            <div class="price-card">
                  <div class="price-header">
                      <h5>3 Months</h5>
                      <div class="d-flex" style="align-items: center; justify-content: center">
                          <sup>US$</sup><h1>14.99</h1>
                      </div>
                  </div>

                  <div class="price-feature">
                      <h5 class="feature-bg">All Robot of your choice</h5>
                      <h5>Top 3 Robot for trading recommendation</h5>
                      <h5 class="feature-bg">Daily Signal Recommendation</h5>
                  </div>

                  <div class="text-center">
                      <button class="price-btn">SUBSCRIBE NOW</button>
                  </div>
              </div>
        </div>
        <div class="col-md-4">
            <div class="price-card">
                  <div class="price-header">
                      <h5>12 Months</h5>
                      <div class="d-flex" style="align-items: center; justify-content: center">
                          <sup>US$</sup><h1>59.88</h1>
                      </div>
                  </div>

                  <div class="price-feature">
                      <h5 class="feature-bg">All Robot of your choice</h5>
                      <h5>Top 3 Robot for trading recommendation</h5>
                      <h5 class="feature-bg">Daily Signal Recommendation</h5>
                  </div>

                  <div class="text-center">
                      <button class="price-btn">SUBSCRIBE NOW</button>
                  </div>

                  <div class="best-offer">BEST OFFER</div>
            </div>
        </div>

    </div> <!-- End Price Card -->

    {{-- Why Agerabot Start --}}
    <div class="container text-center section-home">
        <h2>Why should you choose Agerabot?</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('./images/icon1.svg') }}" alt="icon" class="section-home-icon">
                      <h5>NO MARKET RESEARCH NEEDED</h5>
                      <p>Everything has been analised and well calculated by our trading robots.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('./images/icon2.svg') }}" alt="icon" class="section-home-icon">
                      <h5>SAVE TRADING TIME</h5>
                      <p>Trading robots will automatically execute your transactions.</p>
                    </div>
                  </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('./images/icon3.svg') }}" alt="icon" class="section-home-icon">
                      <h5>TRADING'S PSYCHOLOGY AVOIDED</h5>
                      <p>Your transactions become more neat and keep the over-transaction away</p>
                    </div>
                  </div>
            </div>
        </div>
    </div>
    {{-- Why Agerabot End --}}

    {{-- Six Indication Start --}}
    <div class="container indication">
        <h2>6 things that indicate that you need our trading robot</h2>
        
        <div class="row indication-row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('images/icons/things-1.svg') }}" alt="img" class="indication-img">
                      <h5>Tidak punya banyak waktu untuk trading</h5>
                    </div>
                  </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('images/icons/things-2.svg') }}" alt="img" class="indication-img">
                      <h5>analisa pasar yang kurang tepat</h5>
                    </div>
                  </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('images/icons/things-3.svg') }}" alt="img" class="indication-img">
                      <h5>kesulitan dalam menentukan range stop loss yang tidak menentu</h5>
                    </div>
                  </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('images/icons/things-4.svg') }}" alt="img" class="indication-img">
                      <h5>Bingung dalam menentukan angka take profit</h5>
                    </div>
                  </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('images/icons/things-5.svg') }}" alt="img" class="indication-img">
                      <h5>sering melakukan transaksi yang berlebihan</h5>
                    </div>
                  </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                      <img src="{{ asset('images/icons/things-6.svg') }}" alt="img" class="indication-img">
                      <h5>mengalami kerugian yang besar di trading sebelumnya</h5>
                    </div>
                  </div>
            </div>
        </div>
    </div>


    {{-- <div class="row my-5"> <!-- Start Plan Card -->

        <div class="col-md-4">
            <div class="plan-card">
                <div class="plan-header">
                    <h4>All in Plan</h4>
                    <div>
                        <h5>27.96</h5>
                        <h4>(20% off)</h4>
                        <h3>22.39</h3>
                    </div>
                </div>

                <div class="plan-feature-container">
                    <div class="plan-feature">
                        <p>Installment</p>
                        <p style="font-weight: 700">1.99</p>
                    </div>
                    <div class="plan-feature" style="background: none">
                        <p>Maintenance</p>
                        <p style="font-weight: 700">0.99</p>
                    </div>
                    <div class="plan-feature">
                        <p>VPS</p>
                        <p style="font-weight: 700">19.99</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="plan-card">
                <div class="plan-header">
                    <h4>All in Plan</h4>
                    <div>
                        <h5>79.99</h5>
                        <h4>(20% off)</h4>
                        <h3>63.92</h3>
                    </div>
                </div>

                <div class="plan-feature-container">
                    <div class="plan-feature">
                        <p>Installment</p>
                        <p style="font-weight: 700">1.99</p>
                    </div>
                    <div class="plan-feature" style="background: none">
                        <p>Maintenance</p>
                        <p style="font-weight: 700">0.99</p>
                    </div>
                    <div class="plan-feature">
                        <p>VPS</p>
                        <p style="font-weight: 700">19.99</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="plan-card">
                <div class="plan-header">
                    <h4>All in Plan</h4>
                    <div>
                        <h5>313.63</h5>
                        <h4>(20% off)</h4>
                        <h3>250.92</h3>
                    </div>
                </div>

                <div class="plan-feature-container">
                    <div class="plan-feature">
                        <p>Installment</p>
                        <p style="font-weight: 700">1.99</p>
                    </div>
                    <div class="plan-feature" style="background: none">
                        <p>Maintenance</p>
                        <p style="font-weight: 700">0.99</p>
                    </div>
                    <div class="plan-feature">
                        <p>VPS</p>
                        <p style="font-weight: 700">19.99</p>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End Plan Card --> --}}
    
    {{-- <div class="text-center">
        <button class="subscribe-btn mb-4">ROBOT DETAIL</button>
        <button class="subscribe-btn mb-3">LEARN HOW TO USE</button>
    </div> --}}

</div> <!-- End Container -->

@endsection