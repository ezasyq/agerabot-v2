<nav class="navbar navbar-expand-lg fixed-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('homePage') }}">
        <img src="{{ asset('./images/logo.svg') }}" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{ route('homePage') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('robotPage') }}">Trading Robot</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('subsPage') }}">Subscription</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" style="display: flex; justify-content: center; align-items:center" href="#"><i class="fas fa-user-circle mr-3" style="font-size: 24px"></i> Username</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <img src="{{ asset('images/icons/cart.svg') }}" alt="">
            </a>
          </li>
      </div>
    </div>
</nav>