<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-title">
                    <h6>ADDRESS</h6>
                    <p>Jl. Alam Sutera Boulevard <br> Tangerang Selatan, Banten, 15345, <br> Indonesia</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-title">
                    <h6>AGERABOT</h6>
                    <ul>
                        {{-- <li><a href="#">About Us</a></li>
                        <li><a href="#">Support</a></li> --}}
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Disclaimer</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="footer-title">
                    <h6>FOLLOW US</h6>
                    <div class="soc-med">
                        <a class="fb" href="https://www.facebook.com/agerabot" target="_blank"><i class="fab fa-facebook-square"></i></a>
                        <a class="ig" href="https://www.instagram.com/agerabot" target="_blank"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <p style="color: white">Copyright © 2021. Agerabot.com</p>
                </div>
            </div>
        </div>
    </div>
</footer>